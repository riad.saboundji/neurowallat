import React from 'react'
import { Icon, Form, Input, Modal, message } from 'antd';


class PassModal extends React.Component {

    constructor(props) {
        super(props);
    }

    componentWillReceiveProps(newprops) {
        this.setState({ visible: newprops.visible })
    }

    state = {
        visible: false,
        confirmLoading: false
    }

    handleOk = () => {

        this.props.form.validateFields((err, values) => {
            if (!err) {
                this.setState({
                    ModalText: 'Signature of transaction ...',
                    confirmLoading: true,
                });
                this.props.baseapi.signtrx(values.password).then(res => {
                    this.setState({
                        ModalText: 'Sending transactions ...',
                        confirmLoading: true,
                    });
                    console.log(res)
                    this.props.baseapi.sendtrx(res).then(res => {
                        this.setState({
                            visible: false,
                            confirmLoading: false
                        })
                        this.props.cancel()
                        message.success("Send Transaction success")
                    }).catch(err => {
                        this.setState({
                            visible: false,
                            confirmLoading: false
                        })
                        this.props.cancel()
                        message.error("Can Send Transaction")
                    })

                }).catch(err => {
                    message.error(err);
                    this.setState({
                        confirmLoading: false,
                    });
                })
            }
        })

    }

    handleCancel = () => {
        console.log('Clicked cancel button');
        this.setState({
            visible: false,
        });
        this.props.cancel();
    }

    render() {
        const { visible, confirmLoading, ModalText } = this.state;
        const { getFieldDecorator } = this.props.form;
        return (
            <Modal
                title="Password"
                visible={visible}
                onOk={this.handleOk}
                confirmLoading={confirmLoading}
                onCancel={this.handleCancel}
            >
                {confirmLoading && (<p>{ModalText}</p>)}
                {!confirmLoading && (
                    <div>
                        <Form.Item>
                            {getFieldDecorator('password', {
                                rules: [{ required: true, message: 'Please input your Private Password! ' }],
                            })(
                                <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Private Password" />
                            )}
                        </Form.Item>
                    </div>
                )}
            </Modal>
        )
    }
}

export default Form.create({ name: 'set_pass_private' })(PassModal);