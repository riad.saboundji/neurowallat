import React from 'react'
import { Row, Col, Icon, Card, message, Form, Input, Select, Button, Modal } from 'antd';
import Keys from '../../utils/keys'
import BackApi from '../../utils/backapi'
import { Redirect } from 'react-router-dom'
import QRCode from 'qrcode.react';
import PassModal from './PassModal';

const { Option } = Select;

class Keyinfo extends React.Component {

    state = {
        send: false,
        receive: false,
        visible: false
    }

    showModal = () => {
        this.setState({
            visible: true,
        });
    }

    hideModal = () => {
        this.setState({
            visible: false,
        });
    }

    doSign = (pass) => {
        return BackApi.signtrx(pass);
    }

    doSend = (data) => {

    }

    downloadKey(item) {
        let dataStr = JSON.stringify(item);
        let dataUri = 'data:application/json;charset=utf-8,' + encodeURIComponent(dataStr);

        let exportFileDefaultName = item.addr + '.json';

        let linkElement = document.createElement('a');
        linkElement.setAttribute('href', dataUri);
        linkElement.setAttribute('download', exportFileDefaultName);
        linkElement.click();

        message.success('The export of Key is success');
    }

    hasErrors(fieldsError) {
        return Object.keys(fieldsError).some(field => fieldsError[field]);
    }

    handleSend = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
                BackApi.buildtrx(values).then(res => {
                    BackApi.presigntrx(res.data);
                    this.showModal();
                    this.setState({ send: false })
                }).catch(err => {
                    message.error('Error Can\'t build transaction');
                    this.setState({ send: false })
                })

            }
        });
    }

    numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    render() {
        if (Keys.isEmptyWallet())
            return (<Redirect to="/new"></Redirect>)

        let itemkey = Keys.getSelectKey();
        let keydata = Keys.getKeyData(itemkey.addr);
        let showaddr = "--";
        if (itemkey.addr_xy !== undefined) {
            showaddr = itemkey.addr_xy.data
        } else
            setTimeout(() => { this.setState({ updateadd: true }) }, 1000);


        const { send, receive, visible } = this.state;
        const { getFieldDecorator } = this.props.form;

        const prefixSelector = getFieldDecorator('crypto', {
            initialValue: 'ncc',
        })(
            <Select style={{ width: 80 }}>
                <Option value="ncc">NCC</Option>
            </Select>
        );

        const ismobile = global.mobileversion;

        return (
            <Col span={ismobile?24:12}>
                {/* Buttons Send/Recives
                <Row type="flex" justify="space-around" align="middle" style={{ marginTop: '20px' }}>
                    <Col span={6} align="middle">
                        <img src="/ncc.png" width="48px" height="48px" />
                    </Col>
                </Row>*/}
                <Row type="flex" justify="space-around" align="middle">
                    <Col span={24} align="middle" style={{ marginTop: '30px' }}>
                        <span style={{ fontFamily: 'Inconsolata, monospace', fontSize: '42px' , color :"#FFF" }}>{this.numberWithCommas(parseInt(keydata.ncc))} <img src="./ncc.png" width="32px" height="32px" /></span>
                    </Col>
                </Row>

                <Row type="flex" justify="space-around" align="middle" style={{ marginTop: '5px' }}>
                    <Col span={24} align="middle" >
                        <span style={{ fontFamily: 'Inconsolata, monospace', fontSize: '16px' , color :"#FFF" }}>Current Value in USD</span>
                    </Col>
                    <Col span={12} align="middle">
                        <span style={{ fontFamily: 'Inconsolata, monospace', fontSize: '16px', fontWeight: 'bold'  , color :"#FFF"}}>$142.12</span>
                    </Col>
                </Row>


                <Row type="flex" justify="space-around" align="middle" style={{ marginTop: '20px' }} >
                    <Col span={9} align="middle">
                        <Button style={{ width: '130px' }} onClick={(e) => this.setState({ send: true })}><Icon type="logout" /> Send</Button>
                    </Col>
                    <Col span={9} align="middle">
                        <Button style={{ width: '130px' }}><Icon type="login" /> Receive</Button>
                    </Col>
                </Row>

                <Row type="flex" justify="space-around" align="middle" style={{ marginTop: '20px' }}>
                    {!send && !receive &&
                        (<Col span={20} align="middle">
                            <Card
                                style={{ width: '100%', borderRadius: '10px' }}
                            >
                                <QRCode value={showaddr} level="M" size={128} />
                                <p style={{ fontFamily: 'Inconsolata, monospace', fontSize: "12px", marginTop: '10px' }} >{showaddr}</p>
                                <p>Use this address for Send or Receive NeuroChain Money</p>
                                <p>
                                    <Button onClick={(e) => this.downloadKey(itemkey)} style={{ background: '#fafafa' }}><Icon type="export" /> Export your keys pairs</Button>
                                </p>
                            </Card>
                        </Col>)}
                    {send &&
                        (
                            <Col span={20} align="middle">
                                <Card style={{ width: '100%', borderRadius: '10px' }}>
                                    <Form onSubmit={this.handleSend}>
                                        <Form.Item>
                                            {getFieldDecorator('toaddr', {
                                                rules: [{ required: true, message: 'Please input receiving address!' }],
                                            })(
                                                <Input prefix={<Icon type="qrcode" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Enter Receiving Address" />
                                            )}
                                        </Form.Item>
                                        <Form.Item
                                            extra={<p><Icon type="info-circle" /> Spendable : {this.numberWithCommas(parseInt(keydata.ncc))} NCC</p>} //"Spendable : 120,000 NCC"
                                        >
                                            {getFieldDecorator('amount', {
                                                rules: [{ required: true, message: 'Please input amount' }],
                                            })(
                                                <Input addonBefore={prefixSelector} style={{ width: '100%' }} placeholder="Enter Amount" />
                                            )}
                                        </Form.Item>
                                        <Form.Item>
                                            <Button type="primary" htmlType="submit">Send Now</Button>
                                            <Button style={{ marginLeft: 8 }} onClick={(e) => this.setState({ send: false })}>Cancel</Button>
                                        </Form.Item>
                                    </Form>
                                </Card>
                            </Col>
                        )
                    }
                </Row>
                <PassModal
                    visible={visible}
                    cancel={this.hideModal}
                    itemkey={itemkey}
                    baseapi={BackApi} />
            </Col>
        )
    }
}

export default Form.create({ name: 'send_receive' })(Keyinfo);