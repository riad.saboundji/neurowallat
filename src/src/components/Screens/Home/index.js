import React from 'react'
import { Row, Col, Card, List, Carousel } from 'antd';
import { Redirect } from 'react-router-dom'
import Keys from '../../../utils/keys'
import Keyinfo from '../../Keyinfo';

import './style.css';



class Home extends React.Component {

    numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    smalladdr(addr) {
        if (addr === undefined) return "--";
        return addr.substr(0, 8) + '...' + addr.substr(addr.length - 8, 8)
    }

    renderlist(showdata) {
        return (<Row type="flex" justify="space-around" align="middle" style={{ marginTop: '10px' }}>
            <Col span={20}>
                <Card
                    className="listcustom"
                    title={<span style={{ color: 'rgba(0, 0, 0, 0.65)' }}>Transactions History</span>}
                    style={{ width: '100%', borderRadius: '10px' }} >
                    <div style={{ overflow: 'auto', height: '490px', padding: '0 10px' }}>
                        <List
                            dataSource={showdata}
                            renderItem={item => (
                                <List.Item key={item.id}>
                                    <List.Item.Meta
                                        title={item.date}
                                        description={<span >{item.status}</span>}
                                    />
                                    {item.value > 0 &&
                                        (<div style={{ color: '#328cd5', fontSize: '18px', fontFamily: 'Inconsolata, monospace' }}>+{this.numberWithCommas(item.value)} ncc</div>)
                                    }
                                    {item.value <= 0 &&
                                        (<div style={{ color: '#f5222d', fontSize: '18px', fontFamily: 'Inconsolata, monospace' }}>{this.numberWithCommas(item.value)} ncc</div>)
                                    }
                                </List.Item>
                            )}
                        />
                    </div>
                </Card>
            </Col>
        </Row>)
    }

    render() {
        var nf = new Intl.NumberFormat();
        if (Keys.isEmptyWallet())
            return (<Redirect to="/new"></Redirect>)

        let itemkey = Keys.getSelectKey();
        let datakey = Keys.getKeyData(itemkey.addr)
        let showdata = []

        const ismobile = global.mobileversion;

        datakey.trx.forEach(element => {
            let outit = null;
            element.outputs.forEach(el => {
                if (el.address.data == itemkey.addr_xy.data)
                    outit = el;
            })

            let input = element.inputs[0].id.data;
            if (input == "") input = "CoinBase"
            else if (element.inputs > 1) input += ",...";

            showdata.push({
                id: element.id.data,
                date: this.smalladdr(element.id.data),
                status: (<a style={{ fontFamily: 'Inconsolata, monospace' }} >From {input}</a>),
                value: parseInt(outit.value.value)
            })
        });

        return (
            <div>
                <Row style={{ display: 'block' }}>
                    {ismobile ?
                        (<Carousel>
                            <div><Keyinfo /></div>
                            <div><Col span={24}>
                                {this.renderlist(showdata)}
                            </Col></div>
                        </Carousel>) :
                        (
                            <div>
                                <Keyinfo />
                                <Col span={12}>
                                    {this.renderlist(showdata)}
                                </Col>
                            </div>
                        )}
                </Row>
            </div>
        )
    }
}

export default Home;