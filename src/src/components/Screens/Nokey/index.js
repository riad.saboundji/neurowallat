import React from 'react'
import { Redirect } from 'react-router-dom'
import { Form, Icon, Input, Button, Row, Col, Card, Switch } from 'antd';
import Keys from '../../../utils/keys'
import BaseApi from '../../../utils/backapi'


class Nokey extends React.Component {

    state = {
        done: false
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values)        
                Keys.insertNewKey(Keys.buildKey(values.encoding, values.password))

                let itemkey = Keys.getSelectKey();
                BaseApi.loadAddr(itemkey.pub)
                this.setState({ done: true })
            }
        });
    }

    render() {

        const ismobile = global.mobileversion;

        const formItemLayout = {
            labelCol: { span: 10 },
            wrapperCol: { span: 10 },
        };
        const { done } = this.state;
        if (done) {
            return (<Redirect to="/"></Redirect>)
        }
        const { getFieldDecorator } = this.props.form;
        return (
            <div >
                <Row type="flex" justify="space-around" align="middle" style={{ height: '100%' }}>
                    <Col span={ismobile?22:12}>
                        <Card
                            style={{ width: '100%', borderRadius: '10px' , marginTop : '150px' }}
                        >
                            <Form onSubmit={this.handleSubmit} className="login-form">
                                <Form.Item
                                    {...formItemLayout}
                                    label="Encoding Private key"
                                    style={{marginBottom:'0px'}}
                                >
                                    {getFieldDecorator('encoding', { valuePropName: 'checked' , initialValue :true })(
                                        <Switch disabled={true} />
                                    )}
                                </Form.Item>
                                <Form.Item
                                    {...formItemLayout}
                                    label="Save Private key"
                                    style={{marginBottom:'0px'}}
                                >
                                    {getFieldDecorator('saveprivate', { valuePropName: 'checked' })(
                                        <Switch />
                                    )}
                                </Form.Item>
                                <Form.Item                                
                                >
                                    {getFieldDecorator('password', {
                                        rules: [{ required: true, message: 'Please input your Password!' }],
                                    })(
                                        <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password For Private key" />
                                    )}
                                </Form.Item>
                                <Form.Item style={{ textAlign: 'center' , marginBottom : '10px' }}>
                                    <Button type="primary" htmlType="submit" className="login-form-button">Generate Key</Button>
                                </Form.Item>
                            </Form>
                        </Card>
                    </Col>
                </Row>
            </div>
        )
    }
}

export default Form.create({ name: 'normal_login' })(Nokey);
