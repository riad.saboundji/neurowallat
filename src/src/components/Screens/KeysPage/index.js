import React from 'react'
import { Row, Col, Card, List, Button, Carousel } from 'antd';
import { Redirect } from 'react-router-dom'
import Keyinfo from '../../Keyinfo'
import Keys from '../../../utils/keys'
import './style.css';

const ButtonGroup = Button.Group;



class KeysPage extends React.Component {
    state = {
        fixekey: null
    }

    smalladdr(addr) {
        if (addr === undefined) return "--";
        return '0x' + addr.data.substr(0, 8) + '...' + addr.data.substr(addr.data.length - 8, 8)
    }

    renderlist(keys) {

        return (<Row type="flex" justify="space-around" align="middle" style={{ marginTop: '10px' }}>
            <Col span={20}>
                <Card
                    className="listcustom"
                    title={<span style={{ color: 'rgba(0, 0, 0, 0.65)' }}>Accounts</span>}
                    style={{ width: '100%', borderRadius: '10px' }} >
                    <div style={{ overflow: 'auto', height: '490px', padding: '0 10px' }}>
                        <List
                            dataSource={keys}
                            renderItem={item => (
                                <List.Item key={item.id}>
                                    <List.Item.Meta
                                        description={<a style={{ fontFamily: 'Inconsolata, monospace' }} onClick={() => this.setState({ fixekey: item })}>{this.smalladdr(item.addr_xy)}</a>}
                                        title={<span >NCC Account</span>}
                                    />
                                    <div>
                                        <ButtonGroup>
                                            {!item.Select && (<Button icon="select" onClick={() => { Keys.setSelectKey(item.addr); this.forceUpdate(); }} />)}
                                            <Button icon="delete" />
                                        </ButtonGroup>
                                    </div>
                                </List.Item>
                            )}
                        />
                    </div>
                </Card>
            </Col>
        </Row>)
    }

    render() {
        if (Keys.isEmptyWallet())
            return (<Redirect to="/new"></Redirect>)

        let keys = Keys.allKeys()

        const ismobile = global.mobileversion;

        return (
            <div>
                <Row style={{ height: '100%', display: 'block' }}>
                    {ismobile ?
                        (<Carousel><div><Keyinfo /></div>
                            <div><Col span={24}>
                                {this.renderlist(keys)}
                            </Col></div>010</Carousel>) :
                        (<div><Keyinfo />
                            <Col span={12}>
                                {this.renderlist(keys)}
                            </Col></div>)
                    }
                </Row>
            </div>
        )
    }
}

export default KeysPage;