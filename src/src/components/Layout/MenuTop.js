import React from 'react'
import { Menu, Icon, Divider } from 'antd';
import { Link, withRouter } from 'react-router-dom'

const SubMenu = Menu.SubMenu;

class MenuTop extends React.Component {


    render() {
        const { selectedKeys } = '0'
        return (
            <Menu
                mode="horizontal"                                
                selectedKeys={[selectedKeys]}                                
                style={{ float: 'right' , background: 'none' , border : 'none' }}
                onClick={(e) => {}}
            >
                <SubMenu title={<Icon type="setting" style={{color : 'white' , fontSize :'20px' }} />} >
                    <Menu.Item key="setting:1"><Link to="/">Home</Link></Menu.Item>
                    <Menu.Item key="setting:2"><Link to="/keys">Accounts</Link></Menu.Item>
                    <Divider/>
                    <Menu.Item key="setting:3"><Link to="/new">New account</Link></Menu.Item>
                    <Menu.Item key="setting:4" disabled><Link to="/import">Import account</Link></Menu.Item>
                </SubMenu>
            </Menu>
        )
    }
}

export default withRouter(MenuTop)