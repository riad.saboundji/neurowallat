import React from 'react'
import { Layout as AntLayout } from 'antd';
import MenuTop from './MenuTop'

import Routes from '../../routes';


const { Header, Content } = AntLayout;

class Layout extends React.Component {


    render() {

        const ismobile = global.mobileversion;
        return (
            <AntLayout className="layout" style={{background:'none'}}>
                <Header style={{ background: 'none', height: '32px', lineHeight: '32px', padding: '0px' }}>
                    <div style={styles.logo}>
                        <img src="./logo.png" width="207px" height="32px"/>
                    </div>
                    <MenuTop />
                </Header>
                <Content style={{ color: '#FFFFFF', height: '568px' }}>
                    <Routes />
                </Content>
            </AntLayout>)
    }
}

const styles = {
    logo: {
        color: '#328cd5',
        width: '210px',
        height: '32px',                
        float: 'left',
        fontSize: '23px',        
        padding: '5px 25px'
    }
};

export default Layout