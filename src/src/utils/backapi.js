import axios from 'axios-jsonp-pro'
import Keys from './keys'
var hash = require('hash.js');

class backapi {

    pretrxdata = null;
    urlBot = "http://localhost:8080";
    urlIndex = 0;

    urlBots = [
        "http://localhost:8080",
        "http://boot0.test.neurochaintech.io:8080",
        "http://boot1.test.neurochaintech.io:8080",
        "http://boot2.test.neurochaintech.io:8080",
        "http://82.124.114.76:8080"
    ];

    testConnexion(r, e) {
        console.log("Look for ", this.urlBots[this.urlIndex])
        axios.get(this.urlBots[this.urlIndex] + "/open_wallet", { timeout: 3000 })
            .then(res => {
                if (res.data.status) {
                    this.urlBot = this.urlBots[this.urlIndex];
                    r(true);
                } else {
                    this.urlIndex++;

                    if (this.urlIndex > this.urlBots.length)
                        e(false)
                    else
                        this.testConnexion(r, e)
                }
            }).catch(err => {
                this.urlIndex++;
                if (this.urlIndex > this.urlBots.length)
                    e(false)
                else
                    this.testConnexion(r, e)
            });
    }

    connexion() {
        return new Promise((r, e) => {
            this.testConnexion(r, e)
        });
    }
    updateTrx() {
        let keys = Keys.allKeys();
        if (keys !== null) {
            keys.forEach(element => {
                if (element.addr_xy === undefined) {
                    this.loadAddr(element.pub);
                }
                axios.post(this.urlBot + "/transactions", element.pub)
                    .then(res => {
                        let ncc = 0;
                        if (res.data.transactions)
                            res.data.transactions.forEach(el => {
                                el.outputs.forEach(otp => ncc += parseInt(otp.value.value))
                            });
                        Keys.setKeyData(element.addr, { ncc: ncc, trx: res.data.transactions || [] })
                    })
                    .catch(err => console.log(err));
            });
        }
    }


    loadAddr(pubkey) {
        axios.post(this.urlBot + "/get_addr_x_y", pubkey)
            .then(response => {
                let itemkey = Keys.getSelectKey();
                itemkey.addr_xy = response.data;
                Keys.insertNewKey(itemkey)
                this.setState({ fakeload: 1 })
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    presigntrx(data) {
        this.pretrxdata = data;
    }

    signtrx(pass) {
        return new Promise((resolve, reject) => {
            let itemkey = Keys.getSelectKey();
            let privatekey = Keys.decodeprivate(itemkey.priv.value, pass);
            // sign trx
            if (privatekey.length > 0) {
                let owrsign = Keys.signMessage(privatekey, this.pretrxdata.rawTransaction);
                var owrbud = Buffer.from(owrsign, 'hex'); // Ta-da  
                let sign64 = owrbud.toString('base64')
                this.pretrxdata.transaction.signatures[0].signature.data = sign64;
                privatekey = "0x0x0x0x0x0x0x0x0"; // Ta-fa
                resolve(this.pretrxdata)
            }
            else reject("Bad Password")
        });
    }

    sendtrx(data) {
        return new Promise((r, e) => {
            axios.post(this.urlBot + "/send_transactions", data)
                .then(res => (res.data === "true") ? r(res) : e("Bad signature"))
                .catch(err => e(err))
        })
    }

    buildtrx(data, cb) {
        let itemkey = Keys.getSelectKey();
        let datakey = Keys.getKeyData(itemkey.addr);
        let trx = {
            id: {
                type: "SHA256",
                data: ""
            },
            inputs: [],
            outputs: [
                {
                    address: {
                        type: "SHA256",
                        data: data.toaddr
                    },
                    value: { "value": data.amount }
                }
            ],
            signatures: [],
            fees: { "value": 0 }
        }
        let amoutdu = parseInt(data.amount);
        datakey.trx.forEach(element => {
            element.outputs.forEach((otp, index) => {
                if (otp.address.data === itemkey.addr_xy.data && amoutdu > 0) {
                    amoutdu -= parseInt(otp.value.value);
                    trx.inputs.push({
                        id: element.id,
                        output_id: index,
                        signature_id: 0
                    })
                }


            });
        });

        return axios.post(this.urlBot + "/build_transactions", {
            publickey: itemkey.pub,
            transaction: trx
        })
    }
}

const BackApi = new backapi();
export default BackApi;