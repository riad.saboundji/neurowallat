

export function storage_set(key, value) {
    localStorage.setItem(key, JSON.stringify(value));
}

export function storage_get(key) {
    let v = localStorage.getItem(key);
    if (v != null) v = JSON.parse(v);
    return v;
}
