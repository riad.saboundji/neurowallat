import { storage_get, storage_set } from './storage'
var hash = require('hash.js');
var CryptoJS = require("crypto-js");


class keys {
    isEmptyWallet() {
        let keys = storage_get("keys");
        return (keys === null || keys.length === 0) ? true : false;
    }

    getSelectKey() {
        let keys = storage_get("keys");
        let selectkey = null;
        keys.forEach(element => {
            if (element.Select) selectkey = element
        });
        selectkey = selectkey || keys[0];
        return selectkey;
    }

    setSelectKey(addr) {
        let keys = storage_get("keys");
        keys.forEach(ele => {
            ele.Select = false;
            if (ele.addr === addr)
                ele.Select = true;
        })
        storage_set("keys", keys)
    }

    /**
     * To out is Base64 of signature composant (r.s)
     * @param {The Private Key} privatekey 
     * @param {The message to sign} data 
     */
    signMessage(privatekey, data) {
        let key = global.ec.keyFromPrivate(privatekey, 'hex');
        let gg = Buffer.from(data,'hex');
        console.log(" ==> " ,gg);
        let msgHach = hash.sha256().update(gg).digest('hex')
        var signature = key.sign(msgHach);
        console.log(signature);

        console.log(" Verifie " , key.verify(msgHach, signature))
        return signature.r.toString('hex',64) + signature.s.toString('hex',64); //signature.toDER("hex").toString();
    }

    encodeprivate(data, secret) {
        let ciphertext = CryptoJS.AES.encrypt(data, secret);
        return ciphertext.toString();
    }

    decodeprivate(data, secrret) {
        let decrypted = CryptoJS.AES.decrypt(data, secrret);
        return decrypted.toString(CryptoJS.enc.Utf8);
    }

    buildKey(enc, pass) {
        let key = global.ec.genKeyPair();
        let pubPoint = key.getPublic();
        let pubkey = { x: pubPoint.getX().toString('hex'), y: pubPoint.getY().toString('hex') }
        let itemkey = {
            pub: pubkey,
            addr: hash.sha256().update(pubkey.x + pubkey.y).digest("hex").substr(0, 32),
            priv: {
                enc: enc,
                value: (enc) ? this.encodeprivate(key.getPrivate("hex"), pass) : key.getPrivate("hex")
            },
            Select: true
        }
        return itemkey;
    }

    insertNewKey(item) {
        let keys = storage_get("keys") || [];
        let indexifexist = -1;
        keys.forEach((element, i) => {
            if (element.addr === item.addr) indexifexist = i;
        })
        if (indexifexist < 0) keys.push(item);
        else keys[indexifexist] = item;
        storage_set("keys", keys)

        this.setSelectKey(item.addr)
        // init date for this key
        storage_set(item.addr, {
            ncc: 0,
            trx: []
        });
    }

    getKeyData(addr) {
        return storage_get(addr)
    }

    setKeyData(addr, data) {
        storage_set(addr, data);
    }

    allKeys() {
        return storage_get("keys")
    }
}

const Keys = new keys();
export default Keys;