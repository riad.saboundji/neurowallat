import React from 'react'
import { Route } from "react-router-dom";

import Home from './components/Screens/Home'
import Nokey from './components/Screens/Nokey'
import KeysPage from './components/Screens/KeysPage'

class Routes extends React.Component {

    render() {

        return (
            <div>
                <Route exact path="/" component={Home} />
                <Route path="/new" component={Nokey} />
                <Route path="/keys" component={KeysPage} />                
            </div>
        )
    }
}

export default Routes