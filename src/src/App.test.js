import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

import renderer from 'react-test-renderer';

test('renders without crashing', () => {
  const div = document.createElement('div');
  var compose = renderer.create(<App />, div);
  let tree = compose.toJSON();
  expect(tree).toMatchSnapshot();


  ReactDOM.unmountComponentAtNode(div);
});


