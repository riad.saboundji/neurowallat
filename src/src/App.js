import React, { Component } from 'react';
import Layout from './components/Layout';
import { Spin , message} from 'antd';
import { HashRouter as Router } from "react-router-dom";
import './App.css';
import 'antd/dist/antd.css'
import BackApi from './utils/backapi';
var EC = require('elliptic').ec;

// Create and initialize EC context
// (better do it once and reuse it)
global.ec = new EC('secp256k1');
global.mobileversion = true;

class App extends Component {

  state = {
    first: false,
    connexion: false
  }

  render() {
    const { first, connexion } = this.state
    if (!first) {
      this.setState({ first: true });
      BackApi.connexion().then(res => {
        BackApi.updateTrx();
        message.success("Connexion to " + BackApi.urlBot )
        this.setState({ connexion: true })
      }).catch( err => {
          message.error("Can't connexion or any bot ...")
      })
    }
    return (
      <Router>
        <div>
          {connexion && (<Layout />)}
          {!connexion && (
            <Spin tip="Connexion...">
              <Layout />
            </Spin>)}
        </div>
      </Router>
    );
  }
}

export default App;
