#!/bin/bash

echo 'alias dup="docker-compose -f '$(pwd)'/docker/docker-compose.yml up -d"' >> ~/.bashrc
echo 'alias ddown="docker-compose -f '$(pwd)'/docker/docker-compose.yml down"' >> ~/.bashrc
echo 'alias yarn="docker exec -it node91 yarn"' >> ~/.bashrc